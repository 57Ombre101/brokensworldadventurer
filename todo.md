#To do

#Version 0.1

 * Defining objects
     + Define region
         - *Contains a list of all the places in the region*
         - Key : (rx, ry, rz) Three coordinates to locate the region in space
         - Attributes weather, places
     + Define places
       	 - Contains objects and characters
         - Key : (px, py) for a location on the region
         - Attributes : name, ways, description
     + Define objects
         - Different attributes varying from objects (weight common to all ?)
     + Define characters
         - Key : id
         - Attributes : name, description, inventory, health (max, current, points to change level), srength (same tuple as health), abilities (list of level in abilities
         - Abilities :
             * strength
             * firearm short
             * f medium
             * f long
             * throw weapon
             * one-handed blade
             * two-handed
             * shield
             * explosives
             * cooking
             * farming
             * fishing
             * hunting
             * climbing
             * carpentry
             * building
             * crafting
             * charism
             * stamina
         - Contain objects (equipped and not...)

 * Save system
     + Creates a save folder containing :
         -  a folder regions, containing every region of the world. If the game grows too big, it could be useful to load only a few regions.
     + *Saving world*
     + Saving characters
     + *Load save*

 * First region
     + Features forest, river, city. Plans already made.
     + 256 tiles *0.4% done*

 * Several commands
     + *look : look at tile (default, options will be added)*
     + *go :  go to a direction, can be shortened n, north, se...* (shortening not done yet)
     + take : take a thing
     + *exit : exit a game*
     + *save : save the game*
     + Implementing a argparse function

 * Editor mode, in order to 'administrate' the world. This is a dev tool.
     + Add tile
     + Add object
     + Either create Ed character, or add 'ed' before a command to specifiy it is an editor command

 * *Name [Done, Broken World's Adventurer]*

 * *Menu [Finished]*
     + *New game*
         - *Asks new name, if exists, ask overwriting*
       	 - *At first, only region 0, tile 1 : void*
       	 - With editor mode, building other regions
     + *Load game*
         - *Enter name*
     + *Exit*


#Later
 * World generation
     + Biome generation
     + Structure generation

 * Add weapons
     + Swords
     + Rifles
     + Explosives

 * Crafting

 * Commands
     + Add message after exiting a game to the main menu
     + Add options to look object (look <object>...)

 * Menu
     + Options
     + Add a way to delete a game