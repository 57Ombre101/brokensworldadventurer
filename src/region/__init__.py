"""
Module defining the region class
"""

class Region:
    def __init__(self, id):
        self.id = id
        self.weather = None
        self.places = [] #list of Places objects

    def loca_player(self):
        for place in self.places:
            if place.is_player_here():
                x,y = place.coord
                return (self.id, x, y)
        return False
            
    def _get_place(self, x, y):
        for tile in self.places:
            if tile.coord == (x,y):
                return tile
        print("Error ! Tile not found.")
        return False
