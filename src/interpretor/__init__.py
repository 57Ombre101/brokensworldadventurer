"""
Defines the interpretor
"""

import cmd
from save import save_all
from alias.go import go_main

class Shell(cmd.Cmd):
    intro = ""
    prompt = "{ "

    def __init__(self, world):
        cmd.Cmd.__init__(self)
        self.world = world
        self.editor = False

    ## Commands ##

    
    def do_editor(self, arg):
        """Grants the user editor's rights. This is used to add tiles."""
        self.editor = True

    def do_exit(self, arg):
        """Exit the game"""
        # prompt if save game before
        t = None
        while t != "y" and t != "n":
            t = input("Save game ? (y or n)\n{ ")
        if t == "y":
            #print(self.world)
            save_all(self.world)
        return True

    def do_go(self, arg):
        """Go to a direction.
Examples :
 * go north
 * go up
"""
        # let's do some parsing
        # then we move the player

        # initial coordinates :
        ri, xi, yi= self.world.loca_player()
        #initial place object :
        place_init = self.world._get_place(ri, xi, yi)

        # then we need to make a list of tuples
        # (directions, coord) of accessible places
        ls_dir = place_init.ways
        rf, xf, yf = go_main(arg, ls_dir)
        if (rf,xf,yf) != (0,0,0):
            dest = self.world._get_place(rf,xf,yf)
            for person in place_init.characters:
                if person.player:
                    player = person
            place_init.characters.pop(place_init.characters.index(player))
            dest.characters.append(player)
            # show new place
            self.do_look("")

    def do_look(self, arg):
        """Print a description of what is looked at

        If there is no argument, it will print the tile 
you are."""
        loca = self.world.loca_player()
        r,x,y = loca
        place = self.world._get_place(r,x,y)
        print(place)

    def do_save(self, arg):
        """Save the game"""
        save_all(self.world)

    def do_noteditor(self, arg):
        self.editor = False
