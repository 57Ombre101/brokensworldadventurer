"""
This module defines the behaviour of the game, the key principles
"""

from interpretor import Shell

def execute(world):
    """Launch the game,"""
    c = Shell(world)
    c.cmdloop()
