"""
Defines the character class, used by both NPCs and players
"""

class Character:
    def __init__(self, name, description, player, health_max, weight, location, strength):
        self.name = name
        self.description = description
        self.player = player
        self.health = health_max
        self.health_max = health_max
        self.health_pts = 0 #points for passing level
        self.weight = weight #weight of character (without equipment
        self.location = location #(region, x, y)
        self.inventory = [] # Object list
        self.strength = strength
        self.pt_firearm_long = 0

        
