import pickle
import os

def save(obj, file):
    """Save the object obj"""
    with open(file, 'wb') as f:
        p = pickle.Pickler(f)
        p.dump(obj)

def load(file):
    with open(file, 'rb') as f:
        u = pickle.Unpickler(f)
        return u.load()

def save_all(world):
    """Save the world !"""
    savename = world.name
    #print(os.getcwd())
    listdir = os.listdir("./save")
    if not(savename in listdir): # si la sauvegarde n'existe pas, elle est créée
        os.mkdir("save/" + savename)
    save(world, "save/" + savename + "/world.sav")
    return True

def load_all(savename):
    listdir = os.listdir("./save")
    if not(savename in listdir):
        print("Save does not exist")
    else:
        return load("./save/" + savename + "/world.sav")
