"""
Defines World class
"""

class World:
    def __init__(self, name):
        """Initializes World
        
        name : str
        regions : list of Region
        """
        self.name = name
        self.regions = []

    def add_region(self, region):
        """Add a new region to the world"""
        self.regions.append(region)

    def __str__(self):
        return self.name

    def loca_player(self):
        """Returns the location of the player

        Returns a tuple (reg, x, y) if player found. Else False"""
        for reg in self.regions:
            loca = reg.loca_player()
            if loca != False:
                return loca
        print("Error ! Player not found !")
        return False

    def _get_place(self, r, x, y):
        """Get the place of coordinates (r, x, y)"""
        for reg in self.regions:
            if reg.id == r:
                return reg._get_place(x,y)
        print("Error ! Place not found.")
        return False
