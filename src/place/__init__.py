"""
Module defining the Place class
"""


class Place:
    """Places class
    """
    def __init__(self,region, coord, name,ways, description):
        """ region : int ; coord: (int, int) ; name : str ; ways : tuple list ex: [("up", (1,7,8))]
        (region,nb) is unique"""
        self.region = region
        self.coord = coord
        self.name = name
        self.ways = ways
        self.description = description
        self.objects = []
        self.characters = []
    
    def __str__(self):
        ls_ways = [w[0] for w in self.ways]
        dirtxt = (', ').join(ls_ways)
        txt = "\t\t~~" + self.name + "~~\n\t" + self.description + "\nDirections : " + dirtxt
        return txt

    def is_player_here(self):
        """Check if player is in this place."""
        for character in self.characters:
            if character.player:
                return True
        return False

                
            
            
