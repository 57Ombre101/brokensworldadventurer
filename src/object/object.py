class object():
    """Defines the class 'object'

    This is a mother class for every object in the game (weapon, table...).
"""
    def _init_(self, name, description):
        """Each object has a name and a description. Both are strings."""
        object.name = name
        object.description = description
    
    def enregistrer(self):
        pass


    
