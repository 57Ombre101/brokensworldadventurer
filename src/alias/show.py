import __init__ as alias

class Show(alias.Alias('show', ['show'])):
    def __init__(self):
        pass
    
    def fun():
        """Function called when show is called"""
        print("No description available")

s = Show()
s.fun()
