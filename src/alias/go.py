"""
Everything concerning the 'go' command.
"""

import argparse

def go_main(arg, ways):
    """This is the function called when 'go' is entered."""
    direction = go_parse(arg)
    if direction == False:
        #(0,0,0) is the invalid direction
        return (0,0,0)
    else:
        coord_dest = None
        for c in ways:
            if c[0] == direction:
                coord_dest = c[1]
        if coord_dest == None:
            print("You can't go ", direction, ".")
            return(0,0,0)
        else:
            return coord_dest


def go_parse(arg):
    ls_arg = arg.strip(" ").split(" ")
    
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)

    parser.add_argument("direction", help = "Direction choosen")
    args = parser.parse_args(ls_arg)
    if args.direction in ["up","down", "east", \
                          "north-east", "north", \
                          "north-west", "west", \
                          "south-west", "south", \
                          "south-east"]:
        return (args.direction)
    else:
        print("Invalid direction")
        return False
