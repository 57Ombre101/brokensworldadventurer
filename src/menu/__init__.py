import cmd
from menu.new import main as nmain
from menu.load import main as lomain
from operation import execute

class Menu(cmd.Cmd):
    intro = """Broken's World Adventurer
1:New Game
2:Load Game
3:Options

4:Exit"""
    prompt = "{ "
    
    #Commands :

    def do_new(self, arg):
        """Initializes a new game"""
        new_game(arg)
        
    def do_1(self, arg):
        """Initializes a new game (Alias for 'new')"""
        self.do_new(arg)

    def do_load(self, arg):
        """Load a game"""
        load_game(arg)

    def do_2(self, arg):
        """Load a game (Alias for 'load')"""
        self.do_load(arg)

    def do_options(self, arg):
        """Enter the options menu"""
        pass

    def do_3(self, arg):
        """Enter the options menu (Alias for 'options')"""
        self.do_options(arg)

    def do_exit(self, arg):
        """Exit the game"""
        print("See you soon...")
        return True

    def do_4(self, arg):
        """Exit the game (Alias for 'exit')"""
        return self.do_exit(arg)

    #


def new_game(arg):
    print("New game !")
    world = nmain()
    execute(world)
    
def load_game(arg):
    print("Loading game")
    world = lomain()
    execute(world)

def launch():
    """Launch main menu"""
    c = Menu()
    c.cmdloop()
