"""
Module defining the generation pf a new game
"""

import os
from world import World
from region import Region
from place import Place
from save import save_all
from character import Character

def main():
    """ """
    choice = "n"
    while choice == "n":
        savename = input('Save name : ')
        # check if name already used
        # if so, ask if overwriting
        listdir = os.listdir("./save")
        #print(listdir)
        if savename in listdir:
            choice = None
            while choice != "y" and choice != "n":
                choice =input("Name already used. Overwrite ? (y or n)\n{ ")
        else:
            choice = "y"
    # now we initializes a World object
    world = World(savename)
    name = input("Player name : ")
    create_regions(world, name)
    #
    # save
    save_all(world)
    return world
    

def create_regions(world, name):
    """Generates the initial regions

    Before having a world generation, creates only tile 0 region 0 and maybe tile 1 region 1."""
    region0 = Region(0)
    region1 = Region(1)
    p00 = Place(0, (0,0), 'Void', [("up", (1,7,8))], "Welcome to this strange place. You are here on your way to the earth. Have a nice journey !")
    p178 = Place(1, (7, 8), 'A tiny path in the plain', [("north", (1,7,7))], "This is a dirty road going on a N-S direction. There is a plain all around, borded by a forest on the South, and what seems to be a town on the East.")
    p177 = Place(1, (7,7), 'A tiny road in a plain', [("south", (1,7,8))], """This is a dirty road going on a North-South direction. It is located in a plain. You can see a forest on the South. There is a town in the East.
The road forks on a path going East.""")
    player = Character(name, "You", True, 20, 65, (0,0,0), 10)
    p00.characters.append(player)
    region0.places.append(p00)
    #need to add that the character is in (0,0)
    region1.places.append(p178)
    region1.places.append(p177)
    world.add_region(region0)
    world.add_region(region1)
    print(p00)
