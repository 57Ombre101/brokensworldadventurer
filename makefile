all : readme readmesrc todo

readme : README.md
	~/scripts/mopymark.py README.md docs/readme.html

readmesrc : src/README
	~/scripts/mopymark.py src/README docs/src.html

todo : todo.md
	~/scripts/mopymark.py todo.md docs/todo.html
