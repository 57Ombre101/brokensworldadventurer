#Broken World's Adventurer

This game has been inspired by the MUD Vancia, and Dwarf Fortress.


##Goal

This game is aimed at becoming a text-RPG in a post-apocalyptic world. An original feature is *work-credits*, that is to say points you earn IRL to be able to play the game.

##File organisation

 * main.py : This is the file to launch.
 * save1.db : This database is a savefile. It contains tables characters, regions, objects and npcs.
 * [[database]]
   * objects.db : contient la définition de tous les objets
 * [[sql]] : folder where sqlite requests are stored. It co
ntains files that are used to create tables, and file neede
d to use sql.
 * [[src]] : This is the folder with the sourcecode.


##Installation and launching

Just grab the repository and type in a terminal ```python3 main.py```